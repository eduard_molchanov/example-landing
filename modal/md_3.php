<div id="md_3" class="uk-modal ">
    <div class="uk-modal-dialog uk-modal-dialog-large a10c">  
        <a href="" class="uk-modal-close uk-close uk-close-alt"></a>

        <div class="uk-modal-header a10s"><h2 class="uk-text-success">Название сайта или организации заказчика </h2></div>
        <div class="uk-grid">

            <div class="uk-width-5-10" >
                <img src="img/portfolio/euromed.in.jpg" alt="" class="a10c">
            </div>
            <div class="uk-width-1-10" ></div>
            <div class="uk-width-4-10" >
                <hr />
                <h1 class="uk-text-center uk-text-primary">Название организации</h1>               
                <hr />
                <p>Туристическое агенство</p>
                <h2 class="uk-text-center"> Выполненные работы:</h2>  
                <p class="uk-text-large"><span class="uk-icon-medium uk-icon-check-square-o "></span> Дизайн</p>
                <p class="uk-text-large"><span class="uk-icon-medium uk-icon-check-square-o "></span> Верстка</p>
                <p class="uk-text-large"><span class="uk-icon-medium uk-icon-check-square-o "></span> Напонение информацией</p>
                <p class="uk-text-large"><span class="uk-icon-medium uk-icon-check-square-o "></span> Настройка контекстной рекламы</p>
                <p class="uk-text-large"><span class="uk-icon-medium uk-icon-check-square-o "></span> SEO</p>
            </div>  
        </div>


        <hr />
        <div class="uk-modal-futer">
            <div class="uk-grid">
                <div class="uk-width-7-10"></div>
                <div class="uk-width-2-10">
                    <a href="" ><h3 class=" uk-text-primary"> www.name_site.com</h3></a>
                </div>
                <div class="uk-width-1-10">
                    <a href="" class=" uk-icon-large uk-icon-internet-explorer "></a>
                </div>
                
            </div>


        </div>

    </div>


</div>