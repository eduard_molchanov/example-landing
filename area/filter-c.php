<!--filter-c - Дизайнеры-->
<div data-uk-filter="filter-c">
    <div class=" a10c" data-uk-scrollspy="{cls:'uk-animation-fade', repeat: true}">
        <div >
            <a href="img/dep/2/olga.jpg" data-uk-lightbox="{group:'group1'}" title="Дизайнер Ольга" class="uk-thumbnail">
                <img src="img/dep/1_olga.png" width="800" height="600" alt="Дизайнер Ольга">
            </a>
        </div>
    </div>
</div>
<div data-uk-filter="filter-c">
    <div class=" a10c" data-uk-scrollspy="{cls:'uk-animation-fade', repeat: true}">
        <div >
            <a href="img/dep/2/katerina.jpg" data-uk-lightbox="{group:'group1'}" title="Дизайнер Катерина" class="uk-thumbnail">
                <img src="img/dep/1_katerina.png" width="800" height="600" alt="Дизайнер Катерина">
            </a>
        </div>
    </div>
</div>
<div data-uk-filter="filter-c">
    <div class=" a10c" data-uk-scrollspy="{cls:'uk-animation-fade', repeat: true}">
        <div >
            <a href="img/dep/2/mila.jpg" data-uk-lightbox="{group:'group1'}" title="Дизайнер Мила" class="uk-thumbnail">
                <img src="img/dep/1_mila.png" width="800" height="600" alt="Дизайнер Мила">
            </a>
        </div>
    </div>
</div>
<div data-uk-filter="filter-c">
    <div class=" a10c" data-uk-scrollspy="{cls:'uk-animation-fade', repeat: true}">
        <div >
            <a href="img/dep/2/michail.jpg" data-uk-lightbox="{group:'group1'}" title="Дизайнер Михаил" class="uk-thumbnail">
                <img src="img/dep/1_michail.png" width="800" height="600" alt="Дизайнер Михаил">
            </a>
        </div>
    </div>
</div>