<nav class="tm-navbar uk-navbar uk-navbar-attached menu">
    <div class="uk-container uk-container-center">
        <a class="uk-navbar-brand uk-hidden-small" href=""><strong>099-123-45-67</strong>             
                            <!--
                                                     <img class="uk-margin uk-margin-remove" src="" width="90" height="30" title="LANDING-WEB" alt="LANDING-WEB">
                                                    -->
        </a>
        <ul class="uk-navbar-nav uk-hidden-small">
            <?php include "n_menu.php"?>
        </ul>
        <a href="#tm-offcanvas" class="uk-navbar-toggle uk-visible-small" data-uk-offcanvas></a>
        <div class="uk-navbar-brand uk-navbar-center uk-visible-small">
 LANDING-WEB
        </div>
</nav>

<div id="tm-offcanvas" class="uk-offcanvas">
    <div class="uk-offcanvas-bar">
        <ul class="uk-nav uk-nav-offcanvas uk-nav-parent-icon" data-uk-nav="{ multiple: true }">
            <?php include "n_menu.php"?>
        </ul>
    </div>
</div>