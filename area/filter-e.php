<!--filter-e - SEO оптимизаторы-->
<div data-uk-filter="filter-e">
    <div class=" a10c" data-uk-scrollspy="{cls:'uk-animation-fade', repeat: true}">
        <div >
            <a href="img/dep/5/andrey.jpg" data-uk-lightbox="{group:'group1'}" title="SEO оптимизатор Андрей" class="uk-thumbnail">
                <img src="img/dep/1_andrey.png" width="800" height="600" alt="SEO оптимизатор Андрей">
            </a>
        </div>
    </div>
</div>
<div data-uk-filter="filter-e">
    <div class=" a10c" data-uk-scrollspy="{cls:'uk-animation-fade', repeat: true}">
        <div >
            <a href="img/dep/5/ivan.png" data-uk-lightbox="{group:'group1'}" title="SEO оптимизатор Иван" class="uk-thumbnail">
                <img src="img/dep/1_ivan.png" width="800" height="600" alt="SEO оптимизатор Иван">
            </a>
        </div>
    </div>
</div>