<!--filter-a - Руководители-->
<div data-uk-filter="filter-a">
    <div class=" a10c" data-uk-scrollspy="{cls:'uk-animation-fade', repeat: true}">
        <div  >
            <a href="img/dep/4/valera.jpg" data-uk-lightbox="{group:'group1'}" title="Руководитель Валера" class="uk-thumbnail">
                <img src="img/dep/1_valera.png" width="800" height="600" alt="Руководитель Валера" >
            </a>
        </div>
    </div>
</div>
<div  data-uk-filter="filter-a">
    <div class=" a10c" data-uk-scrollspy="{cls:'uk-animation-fade', repeat: true}">
        <div  >
            <a href="img/dep/4/antonina.jpg" data-uk-lightbox="{group:'group1'}" title="Руководитель Антонина" class="uk-thumbnail">
                <img src="img/dep/1_antonina.png" width="800" height="600" alt="Руководитель Антонина">
            </a>
        </div>
    </div>
</div>