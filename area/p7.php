<div id="p7" class="uk-block " >
    <h1 class="uk-text-primary uk-text-center p7-h1 " data-uk-scrollspy="{cls:'uk-animation-fade', repeat: true}">Напишите нам</h1>


    <div class="uk-panel p5">
        <div class="uk-grid" data-uk-scrollspy="{cls:'uk-animation-scale-up', repeat: true}">
            <div class="uk-width-1-10" ></div>
            
            <div class="uk-width-4-10" >
                <hr>   

                <form class="uk-form uk-form-stacked " action="" method="POST" id="email_site">


                    <div class="uk-form-row">
                        <label class="uk-form-label uk-text-primary" for="form-1" class="uk-text-large uk-text-bold uk-text-warning uk-text-right">Ваше имя</label>
                        <div class="uk-form-controls">
                            <input id="form-1" placeholder="Напишите Ваше имя" type="text" class="uk-form-large uk-form-width-large a10f" name="name" />
                        </div>
                    </div>

                    <div class="uk-form-row">
                        <label class="uk-form-label uk-text-primary" for="form-2">Ваш E-mail</label>
                        <div class="uk-form-controls">
                            <input id="form-2" placeholder="Напишите Ваш E-mail" type="text" class="uk-form-large uk-form-width-large a10f" name="email" />
                        </div>
                    </div>

                    <div class="uk-form-row">
                        <label class="uk-form-label uk-text-primary" for="form-3">Ваше  сообщение</label>
                        <div class="uk-form-controls">
                            <textarea id="form-3" cols="50" rows="5" placeholder="Оставьте Ваше  сообщение" class="uk-form-large uk-form-width-large a10f" name="text"></textarea>
                        </div>
                    </div>
                    
                    <div class="uk-form-row">

                        <div class="uk-form-controls">
                            <input type="submit"  value="О т п р а в и т ь ___ н а м___ с о о б щ е н и е" class="uk-button uk-button-large  a10f" id="bem" name="submit_email"/>
                        </div>
                    </div>



                </form>
                <!--<img style=" margin:  auto; display: block; height:  100%; max-width: 1024px;" src="lib/page-under-construct/under_construct.png " alt="Реконстукция сайта"/>--> 
            </div>

            
            <div class="uk-width-4-10" >
                 <hr>  
                
                          <img src="img/email-1.jpg" alt="" />
                    
                  
            </div>
            
        </div>      
        
    </div>
</div>
<?php

//	 отправка почты с сайта

if(isset($_POST['submit_email'])){
  	
  	$f_name=trim(strip_tags($_POST['name']));
	$f_email=trim(strip_tags($_POST['email']));
 	$f_text=trim(strip_tags($_POST['text']));
  	
  	$for_email="inetdollar@gmail.com";    
  	$topic="Сообщение с сайта http://landing-web.com";
  	$message="Сообщение с сайта http://landing-web.com"."\r\n\n"." Имя : ".$f_name."\r\n\n"." E-mail : ".$f_email."\r\n\n"." Текст сообщения : "."\r\n".$f_text;
  	$headers = "Content-type: text/plain; charset=utf-8\r\n";
  
 $ok= mail($for_email, $topic,$message,$headers);
  
	if($ok) {echo  "<script>alert('Ваше сообщение успешно отправлено!');</script>";
    header("refresh:0; "); };  // при смене хоста поменять url
  
}

//	 отправка почты с сайта
?>