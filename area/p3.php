<div id="p3" >
    <h1 class="uk-text-center uk-text-success">WEB департамент - наша команда</h1>
    <hr>
    <div class="uk-grid " >
        <div class="uk-width-1-10 "></div>
        <div class="uk-width-8-10 ">
            <ul id="filter" class="uk-subnav uk-subnav-pill ">   
                <li class="uk-active" data-uk-filter=""><a href="" data-uk-scrollspy ="{cls:'uk-animation-fade', repeat: true, delay:600}">Все </a></li> 
                <li data-uk-filter="filter-b"><a href="" data-uk-scrollspy ="{cls:'uk-animation-fade', repeat: true, delay:500}">Менеджеры</a></li>
                <li data-uk-filter="filter-c"><a href="" data-uk-scrollspy ="{cls:'uk-animation-fade', repeat: true, delay:400}">Дизайнеры</a></li>
                <li data-uk-filter="filter-d"><a href="" data-uk-scrollspy ="{cls:'uk-animation-fade', repeat: true, delay:300}">Программисты</a></li>
                <li data-uk-filter="filter-a"><a href="" data-uk-scrollspy ="{cls:'uk-animation-fade', repeat: true, delay:200}">Руководители</a></li>
                <li data-uk-filter="filter-e"><a href="" data-uk-scrollspy ="{cls:'uk-animation-fade', repeat: true, delay:100}">SEO оптимизаторы</a></li>
            </ul>

            <div class="uk-grid-width-small-1-2 uk-grid-width-medium-1-5 uk-grid-width-large-1-10 tm-grid-heights foto " data-uk-grid="{gutter: 20, controls: '#filter'}">

                <?php
                include 'area/filter-a.php';
                include 'area/filter-b.php';
                include 'area/filter-c.php';
                include 'area/filter-d.php';
                include 'area/filter-e.php';
                ?> 
            </div>
        </div>
    </div>
</div>
