 <!--Портфолио - Интернет- магазины-->
<div class="uk-panel p5">
    
<h2 class="uk-panel-title uk-text-center uk-text-success">Интернет- магазины</h2>
    <div class="uk-grid">

        <div class="uk-width-1-10" ></div>

        <div class="uk-width-2-10" >
            <a href="#md_1" data-uk-modal> <img src="img/portfolio/abeautylab.jpg" alt="" class="uk-thumbnail a10c p"/>  </a>             
        </div>

        <div class="uk-width-2-10" >
            <a href="#md_2" data-uk-modal><img src="img/portfolio/aqua-trade.com.jpg" alt="" class="uk-thumbnail a10c p"/> </a>                
        </div>

        <div class="uk-width-2-10" >
            <a href="#md_3" data-uk-modal> <img src="img/portfolio/euromed.in.jpg" alt="" class="uk-thumbnail a10c p"/>  </a>               
        </div>

        <div class="uk-width-2-10" >
            <a href="#md_4" data-uk-modal> <img src="img/portfolio/florange.co.jpg" alt="" class="uk-thumbnail a10c p"/> </a>               
        </div>

    </div>      

    <div class="uk-grid">

        <div class="uk-width-1-10" ></div>

        <div class="uk-width-2-10" >               
            <a href="#md_5" data-uk-modal> <img src="img/portfolio/ladyukraine.jpg" alt="" class="uk-thumbnail a10c p"/></a> 
        </div>

        <div class="uk-width-2-10" >             
            <a href="#md_6" data-uk-modal><img src="img/portfolio/postelko.com.jpg" alt="" class="uk-thumbnail a10c p"/></a> 
        </div>

        <div class="uk-width-2-10" >               
            <a href="#md_7" data-uk-modal> <img src="img/portfolio/pro-shop.kiev.jpg" alt="" class="uk-thumbnail a10c p"/></a> 
        </div>

        <div class="uk-width-2-10" >               
            <a href="#md_8" data-uk-modal> <img src="img/portfolio/stantorg.com.jpg" alt="" class="uk-thumbnail a10c p"/></a> 
        </div>

    </div>      


</div>
<?php
include 'modal/md_1.php';
include 'modal/md_2.php';
include 'modal/md_3.php';
include 'modal/md_4.php';
include 'modal/md_5.php';
include 'modal/md_6.php';
include 'modal/md_7.php';
include 'modal/md_8.php';
?>