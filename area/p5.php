<div id="p5" class="uk-block-primary">
    <h1 class="uk-text-contrast uk-text-center" data-uk-scrollspy="{cls:'uk-animation-fade', repeat: true}">Отзывы наших клиентов.</h1>
    <br><hr /><br>
    <div class="uk-panel p5">
        <div class="uk-grid">
            <div class="uk-width-1-10" ></div>
            <div class="uk-width-2-10" data-uk-scrollspy ="{cls:'uk-animation-fade', repeat: true, delay:300}">

                <img src="img/2.jpg" alt="" class="uk-border-circle a10c im" id="im_1" />
            </div>
            <div class="uk-width-2-10" data-uk-scrollspy="{cls:'uk-animation-fade', repeat: true, delay:400}">
                <img src="img/4.jpg" alt="" class="uk-border-circle im" id="im_2" />
            </div>
            <div class="uk-width-2-10" data-uk-scrollspy="{cls:'uk-animation-fade', repeat: true, delay:500}">
                <img src="img/6.jpg" alt="" class="uk-border-circle im" id="im_3" />
            </div>
            <div class="uk-width-2-10" data-uk-scrollspy="{cls:'uk-animation-fade', repeat: true, delay:600}">
                <img src="img/8.jpg" alt="" class="uk-border-circle im" id="im_4" />
            </div>
        </div>
    </div>

    <hr />

    <div class="uk-grid">
        <div class="uk-width-1-10"></div>
        <div class="uk-width-8-10 uk-text-contrast  uk-text-large">
            <div class="uk-panel">

                <div id="im_1_d" class="uk-animation-scale-up">
                    <h2 class="uk-text-contrast ">Катерина</h2>
                    <p>
      Было приятно с Вами сотрудничать. Сотрудники компании всегда вежливы, исполняют все профессионально и в оговоренные сроки.
                    </p>
                </div>
                <div id="im_2_d" class="uk-animation-scale-up">
                    <h2 class="uk-text-contrast ">Светлана</h2>
                    <p>
       Могу написать только благодарственный отзыв за проделанную работу. Сайт отлично работает благодаря Вашей технической поддержке,
	а количество моих клиентов растет с каждым днем.
	Очень приятно, что моей юридической помощью сегодня сможет воспользоваться огромное количество людей.
                    </p>
                </div>
                <div id="im_3_d" class="uk-animation-scale-up">
                    <h2 class="uk-text-contrast ">Анастасия</h2>
                    <p>
       Хотим высловить свою благодарность за разработанный сайт и за его качественную оптимизацию.
	Очень приятно с Вами работать. В будущем планируем разработать корпоративный сайт - обязательно к Вам обратимся!
                </div>
                <div id="im_4_d" class="uk-animation-scale-up">
                    <h2 class="uk-text-contrast ">Ольга</h2>
                    <p>
       Очень довольны своим сайтом. Все сделали как нужно, качественно наполнили сайт информацией, а также обучили нашего сотрудника в управлении сайтом.
	Система управления очень понятная, легко работать.
	Также очень приятно то, что в любой момент можно получить консультацию по работе с сайтом, за что большая благодарность!
                    </p>
                </div>
            </div>
        </div>
    </div>
    <hr>
</div>