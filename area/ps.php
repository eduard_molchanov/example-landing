<div id="ps">
    <div class="uk-slidenav-position uk-slider-container" data-uk-slideshow="{kenburns:true, autoplay:true}">
        <ul class="uk-slideshow uk-overlay-active" >
            <li>
                <img src="/img/as3.jpg"  height="400" alt="">
                <div class="uk-overlay-panel uk-overlay-background uk-overlay-fade uk-flex uk-flex-center uk-flex-middle uk-text-center">
                    <div>
                        <h3> Создание сайтов </h3>
                        <p>Мы профессионалы своего дела по созданию, поддержке, продвижению сайтов по всему Миру. <br>
                            У нас работают специалисты в области веб-дизайна, программирования, seo, маркетинга. </p>
                        <!--<button class="uk-button uk-button-primary">Подробнее ...</button>-->
                    </div>
                </div>
            </li>
            <li >
                <img src="/img/as2.jpg"  height="400" alt="">
                <div class="uk-overlay-panel uk-overlay-background uk-overlay-top uk-overlay-slide-top">
                    <h3>Google AdWords контекстная реклама Гугл Адвордс </h3>
                    <p>Это самая быстрая раскрутка сайта. Первые клиенты появятся у вас уже через полчаса после запуска кампании .</p>
                    <!--<button class="uk-button uk-button-primary">Подробнее ...</button>-->
                </div>
            </li>
            <li>
                <img src="/img/as5.jpg"  height="400" alt="">
                <div class="uk-overlay-panel uk-overlay-background uk-overlay-bottom uk-overlay-slide-bottom">
                    <h3>Дизайн сайтов</h3>
                    <p>Встречают по одежке, а провожают…. <br> Так же можно сказать и про сайты. Каким будет первое впечатление посетителя, привлечение внимания к 
                        вашему сайту и главное сможете ли удержать его на сайте и сделать так, 
                        чтобы он сделал заказаз <br> Дизайн, который мы создаем для сайта, – позитивный имидж вашей компании и хорошее отношение к клиентам. </p>
                    <!--<button class="uk-button uk-button-primary">Подробнее ...</button>-->
                </div>
            </li>
            <li>
                <img src="/img/as1.jpg"  height="400" alt="">
                <div class="uk-overlay-panel uk-overlay-background uk-overlay-left uk-overlay-slide-left uk-width-1-4">
                    <h3>Собственный сайт </h3>
                    <p>Наличие собственного сайта – одно из основных условий успешного развития бизнеса. 
                        С помощью сайта не только увеличивается прибыль, но и пополняется база постоянных клиентов.</p>
                    <!--<button class="uk-button uk-button-primary">Подробнее ...</button>-->
                </div>
            </li>
            <li>
                <img src="/img/as4.jpg"  height="400" alt="">
                <div class="uk-overlay-panel uk-overlay-background uk-overlay-right uk-overlay-slide-right uk-width-1-4">
                    <h3>Продвижение/Реклама сайтов</h3>
                    <p>Мы предлагаем Вам полный комплекс работ по разработке сайтов. <br />
                        Мы всегда идем в ногу со временем и предлагаем своим клиентам решения.</p>
                    <!--<button class="uk-button uk-button-primary">Подробнее ...</button>-->
                </div>
            </li>
            
        </ul>
        <a href="#" class="uk-slidenav uk-slidenav-contrast uk-slidenav-previous" data-uk-slideshow-item="previous" style="color: rgba(255,255,255,0.4)"></a>
        <a href="#" class="uk-slidenav uk-slidenav-contrast uk-slidenav-next" data-uk-slideshow-item="next" style="color: rgba(255,255,255,0.4)"></a>
    </div>

</div>