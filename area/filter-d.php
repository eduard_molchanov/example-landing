<!--filter-d - Программисты-->
<div data-uk-filter="filter-d">
    <div class=" a10c" data-uk-scrollspy="{cls:'uk-animation-fade', repeat: true}">
        <div >
            <a href="img/dep/3/vladislav.jpg" data-uk-lightbox="{group:'group1'}" title="Программист Владислав" class="uk-thumbnail">
                <img src="img/dep/1_vladislav.png" width="800" height="600" alt="Программист Владислав">
            </a>
        </div>
    </div>
</div>
<div data-uk-filter="filter-d">
    <div class=" a10c" data-uk-scrollspy="{cls:'uk-animation-fade', repeat: true}">
        <div >
            <a href="img/dep/3/egor.jpg" data-uk-lightbox="{group:'group1'}" title="Программист Егор" class="uk-thumbnail">
                <img src="img/dep/1_egor.png" width="800" height="600" alt="Программист Егор">
            </a>
        </div>
    </div>
</div>
<div data-uk-filter="filter-d">
    <div class=" a10c" data-uk-scrollspy="{cls:'uk-animation-fade', repeat: true}">
        <div >
            <a href="img/dep/3/larisa.jpg" data-uk-lightbox="{group:'group1'}" title="Программист Лариса" class="uk-thumbnail">
                <img src="img/dep/1_larisa.png" width="800" height="600" alt="Программист Лариса">
            </a>
        </div>
    </div>
</div>