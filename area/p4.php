<div id="p4" class="">
    <h1 class="uk-text-warning uk-text-center " data-uk-scrollspy="{cls:'uk-animation-fade', repeat: true}">Наши тарифные планы</h1>
    <br>
    <div class="">
        <div class="uk-grid">

            <div class="uk-width-1-10" ></div>

            <div class="uk-width-2-10 " data-uk-scrollspy ="{cls:'uk-animation-fade', repeat: true, delay:500}">
                <h2 class="uk-text-primary uk-text-center a12">Лендинг</h2> <hr />
                <p class="uk-text-medium" ><span class="uk-icon-medium uk-icon-check-square-o "></span> Создание LANDING PAGE под ключ</p>                        
                <p class="uk-text-medium" ><span class="uk-icon-medium uk-icon-check-square-o "></span> Подключение WEB-аналитики</p>                        
                <p class="uk-text-medium" ><span class="uk-icon-medium uk-icon-check-square-o "></span> Подключение онлайн помощников</p>  
                <hr />
                <p class="uk-text-large uk-text-danger" ><span class="uk-icon-large uk-icon-shopping-cart uk-text-warning"></span> ___ от 10 000 руб.</p>  
            </div>
            <div class="uk-width-1-10" ></div>

            <div class="uk-width-2-10" data-uk-scrollspy ="{cls:'uk-animation-fade', repeat: true, delay:400}">
                <h2 class="uk-text-primary uk-text-center a12">Корпоративный сайт</h2> <hr />
                <p class="uk-text-medium" ><span class="uk-icon-medium uk-icon-check-square-o "></span> Создание LANDING PAGE под ключ</p>                        
                <p class="uk-text-medium" ><span class="uk-icon-medium uk-icon-check-square-o "></span> Подключение WEB-аналитики</p>                        
                <p class="uk-text-medium" ><span class="uk-icon-medium uk-icon-check-square-o "></span> Подключение онлайн помощников</p> 
                <p class="uk-text-medium" ><span class="uk-icon-medium uk-icon-check-square-o "></span> А/В - тестирование</p> 
                <p class="uk-text-medium" ><span class="uk-icon-medium uk-icon-check-square-o "></span> Настройка Яндекс Директ</p>
                <hr />
                <p class="uk-text-large uk-text-danger" ><span class="uk-icon-large uk-icon-shopping-cart uk-text-warning"></span> ___ от 20 000 руб.</p>  
            </div>
            <div class="uk-width-1-10" ></div>

            <div class="uk-width-2-10" data-uk-scrollspy ="{cls:'uk-animation-fade', repeat: true, delay:300}">
                <h2 class="uk-text-primary uk-text-center a12">Интернет-магазин</h2> <hr />
                <p class="uk-text-medium" ><span class="uk-icon-medium uk-icon-check-square-o "></span> Создание LANDING PAGE под ключ</p>                        
                <p class="uk-text-medium" ><span class="uk-icon-medium uk-icon-check-square-o "></span> Подключение WEB-аналитики</p>                        
                <p class="uk-text-medium" ><span class="uk-icon-medium uk-icon-check-square-o "></span> Подключение онлайн помощников</p> 
                <p class="uk-text-medium" ><span class="uk-icon-medium uk-icon-check-square-o "></span> А/В - тестирование</p> 
                <p class="uk-text-medium" ><span class="uk-icon-medium uk-icon-check-square-o "></span> Настройка Яндекс Директ</p> 
                <p class="uk-text-medium" ><span class="uk-icon-medium uk-icon-check-square-o "></span> Настройка GOOGLE ADWORDS</p> 
                <p class="uk-text-medium" ><span class="uk-icon-medium uk-icon-check-square-o "></span> Настройка РСЯ и КМС</p> 
                <p class="uk-text-medium" ><span class="uk-icon-medium uk-icon-check-square-o "></span> Настройка РЕТАРГЕТИНГА и РЕМАРГЕТИНГА</p> 
                <p class="uk-text-medium" ><span class="uk-icon-medium uk-icon-check-square-o "></span> ГЕОТАРГЕТИНГ и МУЛЬТИЛЕНДИНГ</p>
                <hr />
                <p class="uk-text-large uk-text-danger" ><span class="uk-icon-large uk-icon-shopping-cart uk-text-warning"></span> ___ от 40 000 руб.</p>  
            </div>
        </div>
    </div>            
</div>      

