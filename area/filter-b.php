<!--filter-b - Менеджеры-->
<div data-uk-filter="filter-b">
    <div class=" a10c" data-uk-scrollspy="{cls:'uk-animation-fade', repeat: true}">
        <div  >
            <a href="img/dep/1/marina.jpg" data-uk-lightbox="{group:'group1'}" title="Менеджер Марина" class="uk-thumbnail">
                <img src="img/dep/1_marina.png" width="800" height="600" alt="Менеджер Марина">
            </a>
        </div>
    </div>
</div>
<div data-uk-filter="filter-b">
    <div class=" a10c" data-uk-scrollspy="{cls:'uk-animation-fade', repeat: true}">
        <div  >
            <a href="img/dep/1/igor.jpg" data-uk-lightbox="{group:'group1'}" title="Менеджер Игорь" class="uk-thumbnail">
                <img src="img/dep/1_igor.png" width="800" height="600" alt="Менеджер Игорь">
            </a>
        </div>
    </div>
</div>
<div data-uk-filter="filter-b">
    <div class=" a10c" data-uk-scrollspy="{cls:'uk-animation-fade', repeat: true}">
        <div  >
            <a href="img/dep/1/nikolay.jpg" data-uk-lightbox="{group:'group1'}" title="Менеджер Николай" class="uk-thumbnail">
                <img src="img/dep/1_nikolay.png" width="800" height="600" alt="Менеджер Николай">
            </a>
        </div>
    </div>
</div>

<div data-uk-filter="filter-b">
    <div class=" a10c" data-uk-scrollspy="{cls:'uk-animation-fade', repeat: true}">
        <div  >
            <a href="img/dep/1/irina.jpg" data-uk-lightbox="{group:'group1'}" title="Менеджер Ирина" class="uk-thumbnail">
                <img src="img/dep/1_irina.png" width="800" height="600" alt="Менеджер Ирина">
            </a>
        </div>
    </div>
</div>