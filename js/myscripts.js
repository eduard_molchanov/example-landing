$(document).ready(function(){
	
	$("#email_site").validate ({
		
	rules:{
		
		name:{
			required: true,
		},
		
		email:{
			required: true,
			email: true,
		},
		
		text:{
			required: true,
		},
		
		
	},
	
	
	
	
	messages:{
		
		name:{
			required: "Это поле обязательно для заполнения",
		},
		
		email:{
			required: "Это поле обязательно для заполнения",
			email: "НЕ корректный e-mail адрес ",
		},
		
		text:{
			required: "Это поле обязательно для заполнения",
		},
		
		
	}	
		
		
		
	});  //end of validate


}); //end of ready